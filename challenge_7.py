from challenge_5 import primes

if __name__ == '__main__':
    i = 1
    n = 10001
    res = 0
    for p in primes():
        if i >= n:
            res = p
            break
        i += 1
    print(res)

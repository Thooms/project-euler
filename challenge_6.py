
def diff(n):
    acc = 0
    for j in range(n+1):
        acc += j * j * (j-1) // 2
    return 2 * acc

if __name__ == '__main__':
    """
    Prints the difference of the square of the sum, and the sum of the squares, of
    the first n integers.

    If we unfold \sum{i}^2, we obtain \sum{i^2} + 2 * \sum_{i < j}{i * j} (with i
    capped by our n). Then the difference is equal to 
    2 * \sum_{0 <= i < j <= n}{i * j} which can be computed in O(n) (we know
    how to compute the sum of the first m integers in O(1)).
    """
    print(diff(100))

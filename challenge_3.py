from math import sqrt

def primes():
    l = [2, 3]
    yield 2
    yield 3
    cursor = 5
    while True:
        if not any(cursor % n == 0 for n in l):
            l.append(cursor)
            yield cursor
        cursor += 1

if __name__ == '__main__':
    """Prints the largest prime factor of the number 600851475143.

    Variant of Euler's algorithm, relies on the fact that the prime
    factor decomposition is unique, and that there's only one possible
    factor which is >sqrt(n).

    """

    n = 600851475143
    n_ = n
    last = 2
    for p in primes():
        #print('Studying {}, current reminder is {}'.format(p, n_))
        if last >= sqrt(n) or n_ == 1:
            print(last)
            break
        if n_ % p == 0:
            while n_ % p == 0:
                n_ //= p
            last = p
        
        

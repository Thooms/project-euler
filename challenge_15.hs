import Data.Ratio

f _ 0 = 1
f n_ k = (n_ % k) * f (n_ - 1) (k - 1)

main :: IO ()
main = print . numerator $ f (2*n) n
  where n = 20

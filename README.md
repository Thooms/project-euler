
You can find here my solutions for the Project Euler challenges
(https://projecteuler.net).
I'll try to use as few non-standard libraries as possible, so if
you want to run them, just use the classic CLI tools.

#include <vector>
#include <iostream>

int min(int a, int b) {
  return a < b ? a : b;
}

// use with ./challenge_81 < challenge_81_data.txt
int main(void) {
  int n = 80;
  int m = 80;
  std::vector< std::vector<unsigned long int> > mat(n);

  for (int i = 0; i < n; i++) {
    mat[i] = std::vector<unsigned long int>(m);
    for (int j = 0; j < m; j++) {
      std::cin >> mat[i][j];
    }
  }

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++) {
      if (i == 0) {
        if (j == 0) // (0, 0), we don't do anything
          continue;
        mat[i][j] += mat[i][j-1]; // first line, we only add the left cell
        continue;
      }
      if (j == 0) {
        mat[i][j] += mat[i-1][j]; // left column, we only add the upper cell
        continue;
      }
      mat[i][j] += min(mat[i-1][j], mat[i][j-1]);
    }

  std::cout << mat[79][79] << std::endl;
  return 0;
}

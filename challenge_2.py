

def fib(n):
    """
    Generates the n first numbers of the Fibonacci sequence.
    """
    def fib_():
        a, b = 1, 2
        while a < n:
            yield a
            a, b = b, a + b
    return fib_()

if __name__ == '__main__':
    """
    Prints the sum of the even-valued terms of the Fibonacci sequence which are
    below 4M.

    Here we simply generate the sequence without keeping every term in memory
    (by leveraging Python generators), and we just add the relevant terms to
    our sum.
    """
    s = 0
    for i in fib(4000000):
        if i % 2 == 0:
            s += i
    print(s)

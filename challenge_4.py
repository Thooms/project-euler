
def is_palindrome(n):
    s = str(n)
    return s == s[::-1]

if __name__ == '__main__':
    """
    Yes, a string comparison.
    """
    max_ = 0
    for n in [n * m for n in range(1000) for m in range(1000)]:
        if is_palindrome(n) and n > max_:
            max_ = n

    print(max_)

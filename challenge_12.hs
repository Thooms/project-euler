import Data.List
import Data.Map as M

triangleNumbers :: [Int]
triangleNumbers = unfoldr f (0, 1)
  where f (acc, curr) = Just(acc + curr, (acc + curr, curr + 1))

divisors n = [x | x <- [1..(floor . sqrt . fromIntegral $ n)], n `mod` x == 0]

divisorsNumber n = 2 * (length . divisors $ n)

challenge12 :: Int -> Int
challenge12 k = head [n | n <- triangleNumbers, divisorsNumber n > k]

main = print $ challenge12 500


def sum_(i):
    """
    Returns \sum_{i=0}^{i}{i}
    """
    return (i * (i+1)) // 2

if __name__ == '__main__':
    """
    Prints the sum of all the multiples of 3 or 5 below 1000.
    
    Comes trivially from |A u B| = |A| + |B| - |A `inter` B|.
    """
    s = 3 * sum_(333) + 5 * sum_(199) - 15 * sum_(66)
    print(s)
    

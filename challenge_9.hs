
pyt :: Int -> [(Int, Int, Int)]
pyt n = filter isPythagorean
        [ (a, b, c)
        | a <- [1..n]
        , b <- [a+1..n]
        , c <- [b+1..n]
        ]
  where isPythagorean (a, b, c) = a * a + b * b == c * c

-- We generate all the triplets below 1000, and take the matching one.
main :: IO ()
main = do
  print $ a * b * c
  where triplets = pyt 1000
        results = filter (\(a, b, c) -> (a + b + c == 1000)) triplets
        (a,b,c) = head results

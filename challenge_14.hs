import Data.List
import qualified Data.IntMap.Strict as Map

collatz :: Int -> Int
collatz n | n < 1 = error "Only positive numbers :)"
          | n == 1 = n
          | even n = n `div` 2
          | otherwise = 3 * n + 1

l_collatz :: (Map.IntMap Int, Int) -> (Map.IntMap Int, Int)
l_collatz (mem, n)
  | n == 1 = (Map.fromList [(1, 1)], 1)
  | otherwise =
      case (n `Map.lookup` mem) of
      Just l -> (mem, l)
      Nothing -> let (mem_, m) = l_collatz (mem, (collatz n))
                 in (Map.insert n (m+1) mem_, m+1)


-- which starting number < n gives the longest chain ?
challenge_14 n = foldl maxBy (Map.empty, 0, 0) (reverse [1..n])
  where maxBy (mem, currMax, currMaxI) cand =
          let (mem_, l) = l_collatz (mem, cand)
          in if l > currMax then (mem_, l, cand) else (mem_, currMax, currMaxI)

-- TODO: profiling, this takes way too much time.
main :: IO ()
main = do
  let n = 1000000
      (_, maxLen, maxStart) = challenge_14 n
  putStrLn $ show maxStart ++ " with length " ++ show maxLen

import Data.List

digitsList :: Integer -> [Integer]
digitsList n = unfoldr f n
  where f x | x == 0 = Nothing
            | otherwise = Just (x `mod` 10, x `div` 10)

main :: IO ()
main = print . sum . digitsList $ 2 ^ 1000

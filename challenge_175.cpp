#include <iostream>

unsigned long long rev(unsigned long long n) {
    unsigned long long res = 0, tmp;
    /* Remove trailing zeroes just in case */
    while (n && !(n % 10))
        n /= 10;

    while (n >= 10) {
        tmp = n % 10;
        res += tmp;
        res *= 10;
        n /= 10;
    }
    if (n) res += n % 10;

    return res;
}

bool odd_digits(unsigned long long n) {
    if (!n) return false; /* handle the 0 case */

    while (n) {
        if (!(n % 2)) /* if even digit */
            return false;
        n /= 10;
    }
    return true;
}

unsigned int count_sum_rev(unsigned long long bound) {
    unsigned long long i;
    unsigned int count;
    for (i = 0; i <= bound; i++) {
        if ((i % 10) && odd_digits(i + rev(i))) /* we do not count multiples of 10 */
            count++;
    }
    return count;
}

int main(void) {
    std::cout << count_sum_rev(1000000000) << std::endl;
    return EXIT_SUCCESS;
}

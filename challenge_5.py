from math import sqrt, pow

def primes():
    l = [2, 3]
    yield 2
    yield 3
    cursor = 5
    while True:
        if not any(cursor % n == 0 for n in l):
            l.append(cursor)
            yield cursor
        cursor += 1

def factor(n):
    factors = []
    n_ = n
    last = 1
    for p in primes():
        if last >= sqrt(n) or n_ == 1:
            break
        if n_ % p == 0:
            while n_ % p == 0:
                n_ //= p
                factors.append(p)
            last = p
    out = {}
    for f in factors:
        if f in out:
            out[f] += 1
        else:
            out[f] = 1
    return out

def ppcm(l):
    out = {}
    for factor_dict in l:
        for factor, mult in factor_dict.items():
            if factor in out:
                out[factor] = max(out[factor], mult)
            else:
                out[factor] = mult
    res = 1
    for f, fr in out.items():
        res *= int(pow(f, fr))
    return res

if __name__ == '__main__':
    l = list(range(2,21))
    factorizations = list(map(factor, l))
    print(ppcm(factorizations))
    


